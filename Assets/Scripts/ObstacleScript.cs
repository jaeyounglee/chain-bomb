﻿using UnityEngine;

public enum ObstacleState { Idle, Blown };

public class ObstacleScript : MonoBehaviour
{

    private BombManager bombManager;
    public ObstacleState obstacleState;

    // Use this for initialization
    void Start()
    {
        bombManager = GameObject.Find("Manager").GetComponent<BombManager>();
        obstacleState = ObstacleState.Idle;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (obstacleState == ObstacleState.Blown)
        {
            if (col.gameObject.tag == "Bomb")
            {
                col.gameObject.GetComponent<BombScript>().ExplosionTriggered();
            }

            if (col.gameObject.tag == "Target")
            {
                bombManager.KillTarget();
            }

            if (col.gameObject.tag == "Obstacle")
            {
                if (col.gameObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic)
                    col.gameObject.GetComponent<ObstacleScript>().obstacleState = ObstacleState.Blown;
            }
        }
    }
}
