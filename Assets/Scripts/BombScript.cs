﻿using ExtensionMethods;
using UnityEngine;

public enum BombType { InitBomb, Normal, C4 };

public class BombScript : MonoBehaviour
{
    public BombType thisBombType;
    private float explosionRadius;
    private float explosionForce;
    private BombManager bombManager;
    private float scale;

    // Use this for initialization
    void Start()
    {
        scale = 7.0f;
        bombManager = GameObject.Find("Manager").GetComponent<BombManager>();
        explosionRadius = 14.0f;
        explosionForce = 200.0f;
        if (this.transform.childCount > 1)
        {
            SpriteRenderer sr = this.transform.GetChild(1).GetComponent<SpriteRenderer>();
            sr.transform.localScale = new Vector3(2 * scale * explosionRadius, 2 * scale * explosionRadius);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void C4Explosion()
    {

    }

    public void ExplosionTriggered()
    {
        Vector3 explosionPosition = this.transform.position;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPosition, explosionRadius * (scale+1));
        colliders.SortArraybyDistance(explosionPosition);

        this.gameObject.SetActive(false);
        foreach (Collider2D c in colliders)
        {
            Rigidbody2D rb = c.GetComponent<Rigidbody2D>();


            if (rb != null)
            {
                if (rb == this.transform.GetChild(0).GetComponent<Rigidbody2D>())
                    continue;
                if (rb.gameObject.tag == "Obstacle")
                {
                    rb.AddExplosionForce(explosionForce, explosionPosition, explosionRadius);
                    rb.GetComponent<ObstacleScript>().obstacleState = ObstacleState.Blown;
                }
                else if (rb.gameObject.tag == "Bomb")
                {
                    bombManager.Add2BombQueue(rb.gameObject);
                }
                else if (rb.gameObject.tag == "Target")
                {
                    bombManager.KillTarget();
                }
            }
        }
    }

}
