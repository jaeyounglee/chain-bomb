﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum Selection { NONE, Normal, Init };

public class BombManager : MonoBehaviour
{

    public GameObject initBomb;
    public GameObject normalBomb;
    private GameObject currentSelectBomb;
    public Button initBombButton;
    public Button normalBombButton;
    private Queue<GameObject> BombQueue;
    private float time = 1.0f;
    private Canvas canvas;
    public List<Button> bombs;
    private Selection currentSelect;
    private Button currentSelectButton;
    private Event e;
    private string CurrentScene;

    [SerializeField]
    public int numberofNormalBomb;

    // Use this for initialization
    void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        BombQueue = new Queue<GameObject>();

        currentSelect = Selection.NONE;
        currentSelectBomb = null;
        currentSelectButton = null;

    }

    // Update is called once per frame
    void Update()
    {
        if (BombQueue.Count != 0)
            time -= Time.deltaTime;


        if (time <= 0 && BombQueue.Count != 0)
        {
            for(int i = 0; i < BombQueue.Count; i++)
            {
                GameObject g = BombQueue.Dequeue();
                g.GetComponent<BombScript>().ExplosionTriggered();
            }
            time = 1.0f;
        }

        if (currentSelect != Selection.NONE)
        {
            Vector3 wldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            wldPos.z = 0;
            currentSelectBomb.transform.position = wldPos;

			Vector2 mousePos = wldPos;

			Ray2D r = new Ray2D (wldPos, Vector2.zero);

			RaycastHit2D[] hits = Physics2D.RaycastAll (r.origin, r.direction);
			Debug.Log (hits.Length);

			foreach (RaycastHit2D h in hits)
			{
				if (h.collider.gameObject.tag == "Forbidden")
				{
					Debug.Log ("forbidden");
					goto END;
				}
			}

            if (Input.GetMouseButton(0))
            {
                Vector2 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                if (currentSelect == Selection.Init)
                {
                    currentSelectBomb.transform.position = clickPosition;
                }
                else if (currentSelect == Selection.Normal)
                {
                    currentSelectBomb.transform.position = clickPosition;
                }
                currentSelect = Selection.NONE;
                currentSelectButton.gameObject.SetActive(false);
            }
            else if (Input.GetMouseButton(1))
            {
                currentSelect = Selection.NONE;
                currentSelectButton.gameObject.SetActive(true);
                Destroy(currentSelectBomb);
            }
        }

		END:{}
    }

    public void Add2BombQueue(GameObject g)
    {
        g.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        BombQueue.Enqueue(g);
    }

    public bool CheckBombQueueExistence(GameObject g)
    {
        if (BombQueue.Contains(g))
            return true;

        return false;
    }

    public void StartHolyWar()
    {
        Debug.Log("Start Holy-war");

        GameObject district = GameObject.Find("district");
        if (district != null)
            district.SetActive(false);
        GameObject IO = GameObject.Find("InteractableObstacles");
        Debug.Log(IO);
        Debug.Log(IO.transform.childCount);
        for (int i = 0; i < IO.transform.childCount; i++)
            IO.transform.GetChild(i).GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

        initBomb.GetComponent<BombScript>().ExplosionTriggered();

        foreach (Button g in bombs)
        {
            if (g.gameObject.activeSelf == true)
                g.gameObject.SetActive(false);
        }
    }

    public void NormalButtonListener(Button b)
    {
        b.gameObject.SetActive(false);
        currentSelect = Selection.Normal;
        currentSelectButton = b;
        currentSelectBomb = Instantiate(normalBomb);
    }
    public void InitButtonListener(Button b)
    {
        b.gameObject.SetActive(false);
        currentSelect = Selection.Init;
        currentSelectButton = b;
        currentSelectBomb = Instantiate(initBomb);
    }

    public void Reload()
    {
        CurrentScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(CurrentScene);
    }

    public void KillTarget()
    {
        Debug.Log("Target down, mission success!");
        SceneManager.LoadScene("BackToMain");
    }

    public void EntertheStage(int a)
    {
        SceneManager.LoadScene("Allah" + a.ToString());
    }
}