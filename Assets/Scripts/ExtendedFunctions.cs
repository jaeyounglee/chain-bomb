﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ExtensionMethods
{
	public static class ExtendedFunctions
	{
		public static void AddExplosionForce(this Rigidbody2D rb, float explosionForce, Vector3 exO, float explosionRadius)
		{
			Vector2 explosionOrigin = new Vector2 (exO.x, exO.y);
			Vector2 rbPos = new Vector2 (rb.transform.position.x, rb.transform.position.y);
            Vector2 dir = rbPos - explosionOrigin;
            float wearoff = 1 - (explosionRadius - dir.magnitude);

			rb.AddForce (dir.normalized * explosionForce * wearoff);
		}

        public static void SortArraybyDistance(this Collider2D[] a, Vector3 basePos)
        {
            a = a.OrderBy(_ => -(_.gameObject.transform.position - basePos).magnitude).ToArray();
        }
	}
}
